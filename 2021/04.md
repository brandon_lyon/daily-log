# Boards

### [DE Kanban](https://gitlab.com/groups/gitlab-com/-/boards/2415063) || [DE Sprint](https://gitlab.com/groups/gitlab-com/-/boards/2415116)

[Sprint New](https://gitlab.com/groups/gitlab-com/-/boards/1761580?scope=all&utf8=%E2%9C%93&label_name[]=mktg-inbound&not[label_name][]=mktg-status%3A%3Atriage&not[label_name][]=mktg-status%3A%3Ablocked) || [Sprint Classic](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-website&not[label_name][]=mktg-status%3A%3Atriage&label_name[]=mktg-inbound&assignee_username=brandon_lyon) || [Inbound leaders](https://gitlab.com/groups/gitlab-com/-/boards/2371969) || [DS sprint](https://gitlab.com/groups/gitlab-com/-/boards/2248456?label_name[]=mktg-website%3A%3Adesign-system&label_name[]=mktg-inbound&label_name[]=mktg-website)

# Sticky tasks

[Metrics](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/308) || [Capacity math](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1243) || [Localization Q1](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/271) || [Localization Q2](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/322) || [Slippers](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/218) || [CMS](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/192) || [Commit template](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/275) || [Cookiebot](https://gitlab.com/groups/gitlab-com/-/epics/681) || [Marketo](https://gitlab.com/groups/gitlab-com/-/epics/699) || [www DRI](https://gitlab.com/gitlab-com/Product/-/issues/1869) || [Lost dashboards](https://gitlab.com/gitlab-com/business-technology/team-member-enablement/issue-tracker/-/issues/1522#note_549825069).

# Quick links

[Example epic](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/192) || [Example issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/546) || [Page update](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-update-webpage-brief) || [New page](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-new-website-brief) || [Other](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-other) || [Bug report](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=-website-bug-report)

# Daily template

```
# DAY 2021-00-00

##### Meetings

* List.

##### Work

* Things I planned to do today.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your slack threads, DMs, browser history, and gitlab activity.
```

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/03.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/05.md)

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# FRI 2021-04-30

##### Meetings

* Sanmi's farewell.
* Dr apt.

##### Work

* [Bugfix for product](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/81114).
* MR reviews for HSW.
* Review final MRs for SA.
* [Sid requested pricing copy update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/81135).
* [Review events template for TW](https://gitlab.com/gitlab-com/marketing/inbound-marketing/slippers-ui/-/merge_requests/77#note_565146643).
* [Quote mark mystery](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80991).
* Slack re functional vs imperative programming.

# THU 2021-04-29

##### Work

* Partner pages.
* Cookie consent.
* Review Slippers code and importants.
* Review MRs for HSW.
* [Install page for Todd](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80909).
* Review MRs for MP.
* [Feedback for product](https://gitlab.com/gitlab-org/gitlab/-/issues/328660#note_563108409).
* Slack re issue triage for ZB.

# WED 2021-04-28

##### Notes

* Vaccine day off but still worked.

##### Meetings

* LogRocket.

##### Work

* Slack with SM re workloads.
* Cookie consent.
* [Updated AB test checklist with cross-browser tests](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80878).

# TUE 2021-04-27

##### Notes

* Vaccine half day off.

##### Meetings

* Interview candidate.

##### Work

* Review MRs for HSW.
* [Landing page](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1067).
* [Node 3 migration](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1273).
* [Event logos](https://gitlab.com/gitlab-com/marketing/partner-sponsorships/-/issues/40).
* [Created an engineering AB test checklist](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80671).
* [Homepage banner swap](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1264).

# MON 2021-04-26

##### Meetings

* Sprint planning.
* Sync with MP.

##### Work

* Pricing page release.
* Ruby 3 migration.
* [Content security policy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80517).
* [User survey](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/791).
* Review MRs.
* Review JH user research.
* Developer survey landing page.
* [Google SEO page experience report & Slippers retrospective](https://www.socialmediatoday.com/news/google-adds-new-page-experience-report-to-help-site-owners-prepare-for-al/598683/).

# FRI 2021-04-23

##### Work

* [Cors update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80517).
* Review MRs.
* OneTrust.
* [Pricing page iteration](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80315).
* Assist Jarek with middleman.
* Daily log update.
* Issue triage for TB.
* Help RR with AR.

# THU 2021-04-22

##### Meetings

* Weekly marketing.
* Inbound recap.
* Sprint recap.
* Sprint retro.

##### Work

* Review MRs.
* [Created issue for UX SSoT](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1272).
* [Pricing page iteration](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80315).
* Issue triage.
* [hreflang updates](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80308).
* [Review Ruby 3 upgrade](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78850).

# WED 2021-04-21

##### Notes

* Worked 3 hours extra.

##### Meetings

* DE group conversation.
* Cloudinary sync.

##### Work

* [Hotjar top 5 analysis](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1196).
* Review MRs.
* [Pricing page iteration](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80315).
* Issue triage.
* Localization.

# TUE 2021-04-20

##### Meetings

* DE offsite.

##### Work

* DE offsite.
* Group conversation prep.
* [Issue templates & handbook docs for requesting help](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1263).
* Issue triage.
* [Assist RR with bug](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80170).
* [Slack re glimpses](https://about.gitlab.com/blog/2020/12/02/pre-filled-variables-feature/).
* Slack assist SD with feature flags.

# MON 2021-04-19

##### Meetings

* Smartling sync.

##### Work

* [Issue templates & handbook docs for requesting help](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1263).
* [Journey lines](https://docs.google.com/spreadsheets/d/1QiDQ8a2T_k6nTpoj3d5S0VPzpQO10Sd4YymL715d08o/edit#gid=1501366238).
* [Smartling](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1033).
* Pricing page AB test.
* Prep for meetings.
* Weekly planning and refinement.
* Help SM with Slippers.
* [Pipeline trouble](https://gitlab.slack.com/archives/CVDP3HG5V/p1618861163059400).
* Weekly recap for daily log.

# PTO 2021-04-16

* Family and friends day.

# THU 2021-04-15

##### Meetings

* Weekly marketing.
* Group conversation video.
* Powerpoint party.

##### Work

* Prep for teambuilding.
* [Minor bugfix](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79941).
* [Documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79910).
* [Review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79882) [MRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79910) [for HSW](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79904).
* Create issue for updating how-to-get-help.
* Issue refinement.
* [MR reviews](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79849).
* Slack re books.
* Slack re monorepo for product + handbook sharing.
* Slack re Brand team PDF bug.

# WED 2021-04-14

##### Meetings

* Smartling sync.

##### Work

* [Group conversation slides](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1238).
* Prepare teambuilding presentation.
* [Created AB testing tech issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1260).
* [Created issue for documenting the signup flow](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1257).
* [Quick fix for the FOSS team](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/general/-/issues/214).
* Slack re Sid request.
* Slack re AB test planning.

# TUE 2021-04-13

##### Meetings

* Weekly 1:1.
* Cloudinary.

##### Work

* [MR reviews](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79716).
* [/remind feature request](https://gitlab.com/gitlab-org/gitlab/-/issues/20490).
* [DE bugfix](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79692).
* Weekly recap for daily log.
* [Orange button accessibility](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1255).
* Slack re signup flow.
* Slack re image url SEO bug with lazy loading on Remote Work Report landing page.
* Review pricing page iteration.
* [DE bugfix](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79692).

# MON 2021-04-12

##### Meetings

* Sprint plan sync.

##### Work

* Check in re homepage messaging.
* Slack re Figma budgeting and onboarding.
* Slack re Datastudio splintering.
* Sprint planning.
* Issue triage.
* Prep meeting agendas.

# FRI 2021-04-09

##### Work

* [German localization](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1030) 3 different issues.
* MR reviews.
* Learning Slippers.
* [Create MR checklist for URL redirect projects](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79433).

# THU 2021-04-08

##### Meetings

* Marketing weekly.
* Inbound recap.
* Sprint recap.
* Sprint retro.
* Inbound offsite.
* Sync with LB.

##### Work

* [Metrics OKR refinement](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/308).
* [Pricing page](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/102).
* [Create an epic for DE workflow planning](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/319).
* MR reviews.
* Assist SM with issue interface.
* [Enterprise page copy bug](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79259).

# WED 2021-04-07

##### Meetings

* Weekly 1:1.
* Review docs for MP.
* Update daily log content.
* Update daily log template.
* [Marketo forms](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/19).
* [Typo fix](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79252).
* Heatmaps for TN.

##### Work

* [Metrics OKR refinement](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/308).
* [Community MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79018).
* [Pricing page](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/112).
* [Remove staging](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1239).
* Slack re Pricing copy with AW and JH.

# TUE 2021-04-06

##### Meetings

* Dr apt.

##### Work

* Sprint recap prep.
* Issue refinement.
* Sprint planning prep.
* Documentation.
* MR reviews.
* Slack re Commit.
* [Slack re Oauth](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/76).
* [Marketo API forms](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/19).
* [Created issue for staging environment](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1239).

# MON 2021-04-05

##### Work

* [Created issue for triage buckets](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1235).
* Revised workflow labels.
* [Created issue for per-block metrics](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1236).
* Weekly planning.
* [Assist with Commit videos](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78967).
* [Pricing test copy](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/102).
* MR reviews.
* Documentation.
* [German localization](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1030).
* [Hotjar UX pages](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1196).
* Updated daily log template.

# PTO 2021-04-02

* VTO

# THU 2021-04-01

##### Meetings

* Localization sync.
* Marketing wekly.
* TW coffee chat.
* SM project sync.

##### Work

* [Pricing bug fix](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78833).
* Slack re site separation using node modules.
* Documentation.
* MR reviews.

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/03.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/05.md)
