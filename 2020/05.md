# Boards

* [BML Attention](https://bit.ly/2yqOt8h).
* [Sprint](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-growth&label_name[]=mktg-website&not[label_name][]=mktg-status%3A%3Atriage&assignee_username=brandon_lyon).

# Sticky tasks

* 

# TODO

* 

# Daily template

<details>
<summary>show/hide</summary>

```

# DAY 2020-00-00

##### Notes.

* List.

##### Meetings

* List.

##### Asides

* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
 
##### Primary tasks

* Things I planned to do today.

```

</details>

# [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/06.md)

# FRI 2020-05-29

##### Meetings

* Weekly recap.

##### Asides

* [Check in with product via slack re LaunchDarkly to gather feedback](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51390/).
* Merge request reviews.
* [Update to TOS](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/5739).
* [Assist MOps with URL update](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2745).
* [Add Launch Darkly to tech stack](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/542).
* [Marketo LP 3 column CTA block feasibility](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/3917#note_351490265).
* [Localization docs](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/49009#note_352186048).
* [Review Brand issue templates in Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1589899263925/6d92a8b8a74ac37e528daa99d0c710fd0715da11).
* 360 peer reviews.
 
##### Primary tasks

* [Sprint board milestone updates](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/94).
* [Vendor onboarding](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/93).
* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Default org logos.
    * Update docs.
    * Testimonial section.

# THURS 2020-05-28

##### Meetings

* Weekly marketing tactics.
* Brand and Digital biweekly.
* Jackie re upcoming content & landing pages.

##### Asides

* [Refine the commit landing page video issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7788).
* [UX of blocked resource notice](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/95).
* [Image optimization](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7859) and [website optimization](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7861).
* [Point estimate for solutions template](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/87).
* [Assist with vendor fork merge review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50744).
* [Slack assist with release post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6987#note_330455150).
 
##### Primary tasks

* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Meeting and issue refinement.
        * Updated screenshot.

# WED 2020-05-27

##### Meetings

* Weekly 1:1.

##### Asides

* (1x) Explain why Marketo forms don't load in GDPR regions or when adblocked.
* [Cookiebot UX](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2767).
* Assist ChadW with troubleshooting a build process.
* [Prepare for vendor onboarding](https://gitlab.slack.com/archives/C012U3CASJ2/p1590592156116000) via [issue refinement](https://gitlab.com/groups/gitlab-com/-/boards/1502288?label_name[]=mktg-website&label_name[]=outsource).
    * [Move issues to status WIP or blocked as appropriate per Becky (rreich)](https://gitlab.slack.com/archives/C012U3CASJ2/p1590593136116800?thread_ts=1590592156.116000&cid=C012U3CASJ2).
* Email issue refinement.
* [Team page cookiebot](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7851).
* [Cookiebot test impact of UX changes](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2767#note_350227195).
* Chatted with Sarah re project management and Notion.so.
* [Slack chat with product growth re pricing toggles](https://gitlab.slack.com/archives/C0AR2KW4B/p1590612389281200).
 
##### Primary tasks

* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Arrow & box.

# TUES 2020-05-26

##### Meetings

* [Connect upcoming content & landing pages](https://docs.google.com/document/d/1558nRnuR5k6yiuhDsO2aDhemp1X-9fNnHu8TU1MKBZs/edit).

##### Asides

* [LaunchDarkly procurement](https://gitlab.com/gitlab-com/finance/-/issues/2381#note_347595175).
* (4x) Explain why Marketo forms don't load in GDPR regions or when adblocked.
* [https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50754#note_348656956](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50754#note_348656956)
* [Conversation re workflow and deprecation of status::plan](https://gitlab.slack.com/archives/C012U3CASJ2/p1590511699089700).
* [Pricing page test v2 won out, set as permanent](https://gitlab.slack.com/archives/CQ2G54PU3/p1590512211001200).
* 360 peer reviews.
 
##### Primary tasks

* [Update footer for trademark](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7837).
* [Cookiebot refresh](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7845).
    * Reverted and tried again due to it going horribly wrong.
* [Analytics triggers](https://gitlab.com/gitlab-com/marketing/growth-marketing/web-analytics/-/issues/23).

# MON 2020-05-25

##### Notes

* Holiday Memorial Day.
* Spent 90 minutes on [documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50953/).

# FRI 2020-05-22

##### Meetings

* SSG meeting with ChadW.
* Weekly recap.

##### Asides

* [German cookiebot translation with Indre](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50484).
 
##### Primary tasks

* Sprint end issue refinement.
* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Documentation.

# THURS 2020-05-21

##### Meetings

* Weekly marketing tactics.
* Digital design weekly.
* Marketo templates.

##### Asides

* Conversation with Jensen re pricing page performance.
* Chat with Todd re free users.
* Refine personal backlog.
* Professional CE.
* [Assist ShaneB with handbook MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50715).
* [Survey for MattS](https://docs.google.com/forms/d/e/1FAIpQLScmCCXWWRFiMzc_jJ8NsQrJwRNgFo-jvqQzm01u6KAC9vAn-g/viewform).
 
##### Primary tasks

* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Documentation.

# WED 2020-05-20

##### Meetings

* None

##### Asides

* Issue/email triage.
* [Working with Becky on Mural re issue templates](https://app.mural.co/invitation/mural/gitlab2474/1589899263925?sender=rreich6786&key=9467f856-27a5-42e0-88ca-9c646b1b2205).
* [Pair programming with ChadW](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50398).
 
##### Primary tasks

* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Documentation
    * Added new components.
    * Updated old components.

# TUES 2020-05-19

##### Meetings

* None.

##### Asides

* [Update /resource/ links to the developer survey](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7778).
* [Working with Becky on Mural re issue templates](https://app.mural.co/invitation/mural/gitlab2474/1589899263925?sender=rreich6786&key=9467f856-27a5-42e0-88ca-9c646b1b2205).
* Communicate with LaunchDarkly vendor.
* Field requests related to:
    * Developer survey.
    * Issue templates and workflow management.
* Bugifxes for developer survey.
* [A/B test framework standardization](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/106).
* [Update issue template to account for gated content](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7784).
* [Discussion regarding requiring codeowners](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7755).
* [Advise on programming implementaiton of foreign language URL structure](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2828).
 
##### Primary tasks

* Marketo landing page updates.
    * Only got a chance to update issue description with the next steps and remaining items.

# MON 2020-05-18

##### Notes

* Started work 2.5 hours early today.
* Worked a 9 hour day.

##### Meetings

* ShaneB sync re handbook documentation.

##### Asides

* [Review old just commit code with Chad](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50227).
 
##### Primary tasks

* [DevSecOps survey landing page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7172).
    * Followup work to finish missing items.

# SAT 2020-05-16

* Uploaded the PDF and released the devsecops survey page.
    * Also added link for previous years.

# FRI 2020-05-15

##### Notes

* Worked an extra 4 hrs.

##### Meetings

* Weekly recap.
 
##### Primary tasks

* DevSecOps survey landing page.

# THURS 2020-05-14

* Worked an extra 60 min today.

##### Meetings

* Weekly marketing strategy.
* Biweekly brand & digital.

##### Asides

* Review proposed [growth process](https://docs.google.com/document/d/1upOTNP854uK--1ep83XmHd0lxHP7eWtbEwAitCfUBCE/edit?usp=sharing) & [Team process discussion](https://docs.google.com/document/d/1JU7p6FC0RiEb_BKIx4w3MN_1_vVGSW9nYowIdcTph3I/edit?usp=sharing).
* Fixed ruby crashes on my machine.
    * Disabled rbenv and replaced with rvm.
* [Fixed branch bugs related to code quality bug in master](https://gitlab.slack.com/archives/C62ERFCFM/p1589399385095700).
 
##### Primary tasks

* [DevSecOps survey landing page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7172).
    * Continued work on layout.
    * Started filling in details.

# WED 2020-05-13

##### Meetings

* Weekly 1:1.

##### Asides

* Called Lauren in hospital.
* Assisted John Coughlan with setting up a virtual-events landing page.
* Marketo landing page minor updates after ShaneB review.
 
##### Primary tasks

* [DevSecOps survey landing page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7172).
    * Issue refinement.
    * Started work on layout.

# TUES 2020-05-12

##### Meetings

* [MattS re devsecops survey](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/2636).

##### Asides

* [Provide feedback for product growth team](https://gitlab.com/gitlab-org/growth/product/-/issues/1559#note_341140474).
* [Follow up with stakeholders for marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259#note_339223817).
* [MR assistance](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/48717).
* [Discussion regarding parent group child projects](https://gitlab.slack.com/archives/C012U3CASJ2/p1589305593004900).
* [Make /learn/ sections linkable](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/49501).
* More cookiebot shenanigans.
* [Assist Emily with events bug](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/49535).
* [Assist Agnes with webcast email requirements](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/49506).
 
##### Primary tasks

* Agility | [workflow](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/39) | [assistance](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/5).
* [DevSecOps survey landing page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7172).

# MON 2020-05-11

##### Notes

* VTO but also attended meeting and commented in a few issues.

##### Meetings

* Digital design weekly

# FRI 2020-05-08

##### Meetings

* [Virtual Commit kickoff](https://docs.google.com/document/d/1W5umRU85eEcar-MWNecsbyP6pAKlxMYsaeKCiNRXBr0/edit).
* Growth marketing weekly.

##### Asides

* [Commit updates](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7168#note_339216823).
* [Backlog refinement](https://gitlab.slack.com/archives/G01392P77A9/p1588948269006200).
 
##### Primary tasks

* [LaunchDarkly efficiency](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7651)

# THURS 2020-05-07

##### Meetings

* Marketing group conversation.
* Mareting strategy weekly.

##### Asides

* Review agenda for Friday & Monday.
* Respond to bug reports.
* [Evaluate LaunchDarkly MAU count, contact support, and filed followup issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7651).
* Password management process research.
* [Iframe blocks](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7638).
* [Assist docs team (Craig Norris) with information regarding SwiftType](https://gitlab.slack.com/archives/D012U63DSP9/p1588882768001900).
* [Swifttype administration tasks](https://gitlab.slack.com/archives/DN2S28FNK/p1588884728001900).
* [Side conversation about search analytics](https://gitlab.slack.com/archives/CUJEY21MK/p1588883501091100).
 
##### Primary tasks

* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259)
    * Implemented example featured content block.

# WED 2020-05-06

##### Meetings

* Weekly 1:1
 
##### Asides

* [Outline and file issue for triage vs priority](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/39).
* [Participate in cookiebot issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2767#note_337694444).
* [Helped Joyce with analyst MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7267)
* Pair program issue template for promotion scheduling w/ ShaneB.
* [Assist ShaneR with dataLayer push](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/48185#1f602314b39bff5b11a6cc94ca01a8858f32fcc7)
 
##### Primary tasks

* Backlog refinement.
* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259)
    * Continued work.

# TUES 2020-05-05

##### Notes

* Worked an extra 30 min.

##### Meetings

* Marketo landing page template
* Onboard MattS
 
##### Asides

* [Quicket: case study disclaimer](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/2627)
* [Assist Joyce](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7267)

##### Primary tasks

* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259)
    * Finished trusted-by module.
    * Added speakers module.
    * Rearranged sections and finished section definitions.

# MON 2020-05-04

##### Meetings

* Project management workflow

##### Asides

* [B&D workflow problem statement](https://gitlab.com/gitlab-com/marketing/brand-and-digital/brand-and-digital/-/issues/23)
* [Cookiebot bug](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7507)
* [Personal workflow refinement](https://gitlab.com/brandon_lyon/daily-log)
     * [Investigated IDoneThis](idonethis.com)
* [Personal issue tracking board labels](https://bit.ly/3avynbs)
* [Embedded zoom registration](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2848)
* [Quicket: add topics to nav](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7261)
* [Quicket: document site search](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7395)

##### Primary tasks

* [Marketo landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259)
    * Didn't get a chance to work on it

# FRI 2020-05-01

* Company holiday